module.exports = {
  assetsDir: 'assets',

  devServer: {

    host: 'localhost',
    port: 8080,
    disableHostCheck: true,
    https: false,
    hotOnly: false,

    proxy: {
      '/api': {
        target: 'http://localhost:3000/products',
        changeOrigin: true,
        secure: false
      }
    }
  },

  lintOnSave: undefined,
  publicPath: undefined,
  outputDir: undefined,
  runtimeCompiler: true,
  productionSourceMap: undefined,
  parallel: undefined,
  css: undefined
};