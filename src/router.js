import Vue from 'vue';
import Router from 'vue-router';
import Layout from './views/Layout.vue';

Vue.use(Router);

export default new Router({
  mode: 'history',
  linkActiveClass: 'is-active',
  routes: [{
      path: '/',
      name: 'home',
      component: Layout,
      redirect: '/home',
      children: [{
          path: '/home',
          component: () => import('./views/Home.vue'),
          meta: {
            title: 'YNF Home'
          },
          props: true
        },
      ]
    },
    {
      path: '/collections',
      name: 'collections',
      component: Layout,
      redirect: '/collections/all',
      children: [{
          path: '/collections/product/:id',
          component: () => import('./views/collections/Product.vue'),
          meta: {
            title: ''
          },
          props: true
        },
        {
          path: '/collections/:lineup',
          component: () => import('./views/collections/List.vue'),
          meta: {
            title: ''
          },
          props: true
        },
      ]
    }
  ]
});