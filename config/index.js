module.exports = {
    devServer: {
        proxy: {
            '^/api': {
                target: 'http://localhost:3000/products',
                ws: true,
                changeOrigin: true
            }
        }
    }
}